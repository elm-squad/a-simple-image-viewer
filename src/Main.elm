module Main exposing (..)

import Html exposing (..)
import Html.Events exposing (onCheck, onClick, onInput)
import Html.Attributes exposing (value, src)


main : Program Never Model Msg
main =
    Html.program
        { init = init
        , update = update
        , subscriptions = \_ -> Sub.none
        , view = view
        }


type Msg
    = AddImage
    | SetNewImageURL String
    | SelectedImage String


type alias Model =
    { images : List String
    , selectedImage : Maybe String
    , newImageUrl : Maybe String
    }


init : ( Model, Cmd Msg )
init =
    ( Model [] Nothing Nothing, Cmd.none )


asDef : Maybe String -> String
asDef =
    Maybe.withDefault ""


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        AddImage ->
            let
                justNewUrl =
                    asDef model.newImageUrl
            in
                ( { model
                    | images = justNewUrl :: model.images
                    , newImageUrl = Nothing
                  }
                , Cmd.none
                )

        SelectedImage selectedStr ->
            ( { model | selectedImage = Just selectedStr }
            , Cmd.none
            )

        SetNewImageURL url ->
            ( { model | newImageUrl = Just url }
            , Cmd.none
            )


rederImage : String -> Html Msg
rederImage url =
    li [ onClick <| SelectedImage url ] [ span [] [ text url ] ]


renderImages : List String -> Html Msg
renderImages xs =
    ul [] <| List.map rederImage xs


renderInput : Maybe String -> Html Msg
renderInput ms =
    input
        [ onInput SetNewImageURL
        , value <| asDef ms
        ]
        []


view : Model -> Html Msg
view model =
    div
        []
        [ div
            []
            [ h1 [] [ text "Add img" ]
            , (renderInput model.newImageUrl)
            , button [ onClick AddImage ] [ text "Add" ]
            , (renderImages model.images)
            ]
        , div
            []
            [ h1 [] [ text "View img" ]
            , img
                [ src <| asDef model.selectedImage ]
                []
            ]
        ]
